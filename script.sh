#!/bin/bash

# Spécifiez les informations de connexion à la base de données
DB_USER="root"
DB_PASSWORD="monmotdepasse"
DB_NAME="badass"

# Spécifiez le chemin où le dump de la base de données sera sauvegardé
BACKUP_DIR="/home/oliv/Downloads/dockercompose/kuber/backups"
TIMESTAMP=$(date '+%Y%m%d%H%M%S')

exec >> /home/oliv/Downloads/dockercompose/kuber/backups_log.txt 2>&1

# Exécutez la commande mysqldump pour créer une sauvegarde
# shellcheck disable=SC2024
docker exec -i SQL-01 mysqldump -u$DB_USER -p$DB_PASSWORD $DB_NAME > $BACKUP_DIR/backup_$TIMESTAMP.sql

if [ $? -eq 0 ]; then
    echo "Backup completed successfully at $(date)" >> /home/oliv/Downloads/dockercompose/kuber/backups_log.txt
else
    echo "Backup failed at $(date)" >> /home/oliv/Downloads/dockercompose/kuber/backups_log.txt
fi

echo "Backup completed at $(date)" >> /home/oliv/Downloads/dockercompose/kuber/backups_log.txt